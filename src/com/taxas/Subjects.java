package com.taxas;

import java.util.Map;

public class Subjects {
    private String name;
    private int creditHours;
    private String teacherName;
    private int subjectCode;
    private Map<String,String> questionAnswers;

    public Subjects(String name, int creditHours, String teacherName, int subjectCode, Map<String, String> questionAnswers) {
        this.name = name;
        this.creditHours = creditHours;
        this.teacherName = teacherName;
        this.subjectCode = subjectCode;
        this.questionAnswers = questionAnswers;
    }

    public Subjects() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCreditHours() {
        return creditHours;
    }

    public void setCreditHours(int creditHours) {
        this.creditHours = creditHours;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public int getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(int subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Map<String, String> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(Map<String, String> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }

    @Override
    public String toString() {
        return name + " " + creditHours + " " + teacherName + " " + " "+ subjectCode;
    }
}
