package com.taxas;

public class SpringFactoryMethod {

        private static final SpringFactoryMethod obj=new SpringFactoryMethod();
        private SpringFactoryMethod(){System.out.println("private constructor");}
        public static SpringFactoryMethod getSpringFactoryMethod(){
            System.out.println("factory method ");
            return obj;
        }
        public void msg(){
            System.out.println("hello java studets....");
        }
}
