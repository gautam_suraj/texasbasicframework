package com.taxas;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class StudentDependencyInjectionByConstructor {
    private String name;
    private int rollNo;
    private List<String> subjects;
    private Map<String,String> questionAnswers;

    public StudentDependencyInjectionByConstructor(String name, int rollNo, List<String> subjects,Map<String,String> questionAnswers) {
        this.name = name;
        this.rollNo = rollNo;
        this.subjects = subjects;
        this.questionAnswers = questionAnswers;
    }

   /* public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }*/

  /*  public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }*/

    public StudentDependencyInjectionByConstructor() {
    }

    public void displayInfo(){
        System.out.println(name+" "+rollNo);
        System.out.println("subjects are:");
        for (String subbject: subjects){
            System.out.println(subbject);
        }
        System.out.println(questionAnswers);
        /*Iterator<String> itr=subjects.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }*/
    }
}
